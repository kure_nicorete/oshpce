<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 29.1.18
 * Time: 19:53
 */

namespace App\Forms;

use App\Model\RoleManager;
use App\Model\DuplicateNameException;
use App\Model\Sbory;
use App\Model\SboryManager;
use Nette;
use Nette\Application\UI\Form;

class SboryFormFactory {

    use Nette\SmartObject;

    /** @var FormFactory */
    private $factory;
    /**
     * @var SboryManager
     */
    private $sboryManager;


    public function __construct(FormFactory $factory, SboryManager $sboryManager) {
        $this->factory = $factory;
        $this->sboryManager = $sboryManager;
    }


    /**
     * @param $id_param
     * @param callable $onSuccess
     * @return Form
     */
    public function create($id_param, callable $onSuccess) {
        $form = $this->factory->create();

        $form->addText('name', 'Název Sbory')
            ->setRequired('Prosím vyplńte název.');

        $form->addText('mail', 'E-mail')
            ->setRequired('Prosím vyplńte email.');

        $form->addText('web', 'web');
        $form->addCheckbox("listweb", "Sbor má www stránky");
        $form->addCheckbox("listprapor", "Sbor má prapor");
        $form->addCheckbox("listpripravky", "Sbor má přípravku");
        $form->addCheckbox("listmlzaci", "Sbor má družstvo ml.žáků");
        $form->addCheckbox("liststzaci", "Sbor má družstvo st.žáků");
        $form->addCheckbox("listdorost", "Sbor má družstvo dorostenců");
        $form->addCheckbox("listdorky", "Sbor má družstvo dorostenek");
        $form->addCheckbox("listdorostmix", "Sbor má družstvo dorostu mix");

        if (is_numeric($id_param)) {
            $id = $id_param;
            $sbor = $this->sboryManager->get($id);
            $form['name']->setDefaultValue($sbor->name);
            $form['mail']->setDefaultValue($sbor->mail);
            $form['web']->setDefaultValue($sbor->web);
            $this->explodeCheckboxes($sbor, $form);

            $form->addHidden('id', $id_param);
        }

        $form->addSubmit('send', 'odeslat');

        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
            try {
                $values['bytearray']=$this->getImplodedListFromValues($values);
                $this->sboryManager->save($values);
            } catch (DuplicateNameException $e) {
                $form['nazev']->addError('Tento sbor již existuje.');
                return;
            }
            $onSuccess();
        };

        return $form;
    }

    private function getImplodedListFromValues($values){
        $array = array(
            $values["listweb"] ? "1" : "0",
            $values["listprapor"] ? "1" : "0",
            $values["listpripravky"] ? "1" : "0",
            $values["listmlzaci"] ? "1" : "0",
            $values["liststzaci"] ? "1" : "0",
            $values["listdorost"] ? "1" : "0",
            $values["listdorky"] ? "1" : "0",
            $values["listdorostmix"] ? "1" : "0",
        );
        return implode($array);
    }

    /**
     * @param $sbor
     * @param Form $form
     * @return void
     */
    public function explodeCheckboxes($sbor, Form $form)
    {
        $array = str_split($sbor->byteArray);

        $form["listweb"]->setDefaultValue($array[0] == "1");
        $form["listprapor"]->setDefaultValue($array[1] == "1");
        $form["listpripravky"]->setDefaultValue($array[2] == "1");
        $form["listmlzaci"]->setDefaultValue($array[3] == "1");
        $form["liststzaci"]->setDefaultValue($array[4] == "1");
        $form["listdorost"]->setDefaultValue($array[5] == "1");
        $form["listdorky"]->setDefaultValue($array[6] == "1");
        $form["listdorostmix"]->setDefaultValue($array[7] == "1");
    }

}
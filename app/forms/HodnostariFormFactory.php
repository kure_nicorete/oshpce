<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 29.1.18
 * Time: 19:53
 */

namespace App\Forms;

use App\Model\RoleManager;
use App\Model\HodnostariManager;
use App\Model\DuplicateNameException;
use Nette;
use Nette\Application\UI\Form;

class HodnostariFormFactory {

    use Nette\SmartObject;

    /** @var FormFactory */
    private $factory;

    /**
     * @var RoleManager
     */
    private $roleManager;

    /**
     * @var HodnostariManager
     */
    private $hodnostariManager;

    /**
     * VyborRolesFormFactory constructor.
     * @param FormFactory $factory
     * @param RoleManager $roleManager
     */

    public function __construct(FormFactory $factory, RoleManager $roleManager, HodnostariManager $hodnostariManager) {
        $this->factory = $factory;
        $this->roleManager = $roleManager;
        $this->hodnostariManager = $hodnostariManager;
    }


    public function create($id_param, callable $onSuccess) {
        $form = $this->factory->create();

        $hodnostar = $this->roleManager->findAll();

        $aktualni = "";
        $items = array();
        foreach ($hodnostar as $k) {
            if($k->nazev == "Žádná"){
                $aktualni = $k->getId();
            }
            $items[$k->getId()] = $k->nazev;
        }

        $form->addText('jmeno', 'Jméno')
            ->setRequired('Prosím vyplńte jméno.');
        $form->addText('prijmeni', 'Přijmení')
            ->setRequired('Prosím vyplńte přijmení.');
        $form->addText('telefon', 'telefon')
            ->setRequired('Prosím vyplńte název.');
        $form->addText('email', 'email')
            ->setRequired('Prosím vyplńte název.');
        $form->addText('sdh', 'sdh')
            ->setRequired('Prosím vyplńte název.');
        $form->addSelect('role', 'Funkce: ', array('Funkce' => $items))->setPrompt('Všechny')->setDefaultValue($aktualni);
        $form->addUpload('foto', 'Soubor:')
            ->setRequired(false)
            ->addRule($form::IMAGE, 'Soubor musí být JPEG, PNG nebo GIF.')
            ->addRule($form::MAX_FILE_SIZE, 'Maximální velikost souboru je 5 MB.', 5 * 1024 * 1024);


        if (is_numeric($id_param)) {
            $id = $id_param;
            $hodnostar = $this->hodnostariManager->get($id);
            $form['jmeno']->setDefaultValue($hodnostar->jmeno);
            $form['prijmeni']->setDefaultValue($hodnostar->prijmeni);
            $form['telefon']->setDefaultValue($hodnostar->telefon);
            $form['email']->setDefaultValue($hodnostar->email);
            $form['sdh']->setDefaultValue($hodnostar->sdh);
            $form->addHidden('id', $id_param);
        }


        $form->addSubmit('send', 'vytvorit');


        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
            try {
                $values['zobraz'] = 0;
                $this->hodnostariManager->save($values);
            } catch (DuplicateNameException $e) {
                $form['nazev']->addError('Tato role již existuje.');
                return;
            }
            $onSuccess();
        };


        return $form;
    }
}
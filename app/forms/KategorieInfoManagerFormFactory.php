<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 29.1.18
 * Time: 19:53
 */

namespace App\Forms;

use App\Model\KategorieInfo;
use App\Model\KategorieManager;
use App\Model\Soubor;
use App\Model\SouborManager;
use Nette;
use App\Model\KategorieInfoManager;
use Nette\Security\User;

class KategorieInfoManagerFormFactory {

    use Nette\SmartObject;
    private $OSHPCE = "OSH PCE";
    private $AKTUALNE = "Aktuálně";
    private $HROMADA = "Volební výroční valné hromady SDH a okrsků";
    private $VYSLEDKY = "Výsledky soutěží";
    private $ZAKONIK = "Novela Občanského Zákoníku";
    private $ZASEDANI = "Zasedání představitelů SDH v Černé za Bory";

    private $TISKOPISY = "Tiskopisy";

    /** @var FormFactory */
    private $factory;

    /** @var User */
    private $user;

    /** @var KategorieInfoManager */
    private $kategorieInfoManager;

    /**
     * KategorieInfoManagerFormFactory constructor.
     * @param FormFactory $factory
     * @param User $user
     * @param KategorieInfoManager $kategorieInfoManager
     * @param KategorieManager $kategorieManager
     */
    public function __construct(FormFactory $factory, User $user, KategorieInfoManager $kategorieInfoManager, KategorieManager $kategorieManager) {
        $this->factory = $factory;
        $this->user = $user;
        $this->kategorieInfoManager = $kategorieInfoManager;
    }


    public function create($kategoryID, $subkategoryID, $sekce, callable $onSuccess) {
        $form = $this->factory->create();

        var_dump($kategoryID);
        $url = 'url';
        $file = 'file';
        $upload = 'upload';

        $form->addCheckbox("fileLocal", 'Soubor bude lokálně')
            ->addCondition($form::FILLED)
                ->toggle($file, false)
                ->toggle($upload)
                ->toggle($url, false)
            ->elseCondition()
                ->toggle($file,false)
                ->toggle($upload,false)
                ->toggle($url)
            ->endCondition();

        $form->addText('nazev', 'Zobrazovaný text')
            ->addRule($form::FILLED, 'je nutne vyplnit název')
            ->setOption('id',$file);

        $form->addText('URL', 'URL k souboru')
            ->addRule($form::FILLED, 'je nutne vyplnit url')
            ->setOption('id',$url);

        $form->addUpload('path', 'Cesta k souboru:')
            ->addRule($form::MAX_FILE_SIZE, 'Maximální velikost souboru je 5 MB.', 5 * 1024 * 1024)
            ->addRule($form::FILLED, 'je nutne vybrat soubor')
            ->setOption('id',$upload);

        $kategorie = $this->getKategory();
        $items = array();
        foreach ($kategorie as $key => $value) {
            $items[$key] = $value;
        }
        if($kategoryID == ""){
            $kategoryID = $this->OSHPCE;
        }
        $form->addSelect('kat', 'Kategorie: ', $items)
            ->setDefaultValue($kategoryID);


        $subKategorie = $this->getSubcategory("OSH PCE");
        $items = array();
        foreach ($subKategorie as $key => $value) {
            $items[$key] = $value;
        }
        if($subkategoryID == ""){
            $subkategoryID = $this->AKTUALNE;
        }
        $form->addSelect('subkat', 'Subkategorie: ', $items)
            ->setDefaultValue($subkategoryID);

        $subTri = $this->getSectionKategories();
        $items = array();
        if(count($subTri) != 0){
            foreach ($subTri as $key => $value) {
                $items[$key] = $value;
            }
        }
        $form->addSelect('Sekce', 'Sekce pro OSHPCE aktualne: ', $items);

        $form->onSuccess[] = function($form) use ($onSuccess) {
            $this->successForm($form, $onSuccess);
        };

        return $form;
    }


    private function successForm($form, $onSuccess) {
        $values = $form->getValues();

        $soubor = new Soubor();
        $soubor->nazev = $values['nazev'];
        $soubor->fileLocal = $values['fileLocal'];
        $soubor->url = $values['url'];
        $soubor->path = $values['path'];
        SouborManager::save($soubor);

        $subkat = $this->kategorieInfoManager->findOne('nazev=?', $values['subkat']);
        $kat = $this->kategorieInfoManager->findOne('nazev=?', $values['kat']);
        $sekce = "";
        if(($values['kat'] == $this->OSHPCE) && ($values['subkat'] == $this->AKTUALNE)){
            $sekce = $values['sekce'];
        }
        $kategorie = new KategorieInfo();
        $kategorie->soubor = $this->souboryManager->get($soubor->getId());
        $kategorie->kategorie = $this->kategorieManager->get($kat->id);
        $kategorie->subKategorie = $this->kategorieManager->get($subkat->id);
        $kategorie->sekce = $sekce;

        call_user_func($onSuccess);
    }

    private function getKategory(){
        return array(
            $this->OSHPCE => $this->OSHPCE,
            "ORHS" => "ORHS",
            "ORR" => "ORR",
            "ORP" => "ORP",
            "OROO" => "OROO",
            "ORM" => "ORM",
            "ORH" => "ORH",
            "AZH" => "AZH"
            );
    }

    private function getSubcategory($kategory) {

        switch ($kategory){
            case $this->OSHPCE:
                return array(
                    $this->AKTUALNE => $this->AKTUALNE,
                    $this->TISKOPISY => $this->TISKOPISY,
                    "VV OSH" => "VV OSH",
                    "Zpravodaj" => "Zpravodaj",
                    "HVP" => "HVP"
                    );
                break;
            case "ORHS":
                return array(
                    $this->AKTUALNE => $this->AKTUALNE,
                    $this->TISKOPISY => $this->TISKOPISY,
                    "Plamen" => "Plamen",
                    "Dorost" => "Dorost",
                    "Požární sport" => "Požární sport"
                );
                break;
            case "ORR":
                return array(
                    $this->AKTUALNE => $this->AKTUALNE,
                    $this->TISKOPISY => $this->TISKOPISY,
                    "Odbornosti" => "Odbornosti",
                    "Legislativa" => "Legislativa",
                    "JSDHO" => "JSDHO"
                );
                break;
            case "ORP":
                return array(
                    $this->AKTUALNE => $this->AKTUALNE,
                    $this->TISKOPISY => $this->TISKOPISY,
                    "Odbornosti" => "Odbornosti",
                    "PVČ" => "PVČ",
                    "PO očima dětí" => "PO očima dětí");
                break;
            case "OROO":
                return array(
                    $this->AKTUALNE => $this->AKTUALNE,
                    $this->TISKOPISY => $this->TISKOPISY
                );
                break;
            case "ORM":
                return array(
                    $this->AKTUALNE => $this->AKTUALNE,
                    $this->TISKOPISY => $this->TISKOPISY,
                    "Přípravky" => "Přípravky",
                    "Plamen" => "Plamen",
                    "Dorost" => "Dorost",
                    "LHM" => "LHM",
                    "Učíme se" => "Učíme se",
                    );
                break;
            case "ORH":
                return array(
                    $this->AKTUALNE => $this->AKTUALNE,
                    $this->TISKOPISY => $this->TISKOPISY
                    );
                break;
            case "AZH":
                return array(
                    $this->AKTUALNE => $this->AKTUALNE
                    );
                break;

        }
    }

    private function getSectionKategories() {
            return array(
                $this->AKTUALNE => $this->AKTUALNE,
                $this->ZASEDANI => $this->ZASEDANI,
                $this->ZAKONIK => $this->ZAKONIK,
                $this->HROMADA => $this->HROMADA, $this->VYSLEDKY => $this->VYSLEDKY);
    }

}
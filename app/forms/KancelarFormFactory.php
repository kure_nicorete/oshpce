<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 29.1.18
 * Time: 19:53
 */

namespace App\Forms;

use App\Model\KancelarManager;
use App\Model\DuplicateNameException;
use Nette;
use Nette\Application\UI\Form;

class KancelarFormFactory {

    use Nette\SmartObject;

    /** @var FormFactory */
    private $factory;
    /**
     * @var KancelarManager
     */
    private $kancelarManager;


    public function __construct(FormFactory $factory, KancelarManager $kancelarManager) {
        $this->factory = $factory;
        $this->kancelarManager = $kancelarManager;
    }


    /**
     * @param $id_param
     * @param callable $onSuccess
     * @return Form
     */
    public function create($id_param, callable $onSuccess) {
        $form = $this->factory->create();

        $form->addHidden('name', 'KancelarOSH');

        $form->addText('adresa', 'Adresa')
            ->setRequired('Prosím vyplńte název.');

        $form->addText('email', 'E-mail')
            ->addRule($form::FILLED, 'Nutné vyplnit email');

        $form->addText('telefon', 'Telefon')
            ->setRequired('Prosím vyplńte telefon.');

        $form->addText('location', 'GPS souřadnice')
            ->setRequired('Prosím vyplńte GPS souřadnice.');

        $form->addText('locationLink', 'odkaz do mapy')
            ->setRequired('Prosím vyplńte odkaz do mapy.');

        $form->addText('ico', 'IČO')
            ->setRequired('Prosím vyplńte IČO.');

        $form->addText('bankAccount', 'č. účtu')
            ->setRequired('Prosím vyplńte č. účtu.');

        $form->addText('oteviraciDoba', 'úřední hodiny')
            ->setRequired('Prosím vyplńte otevíraí dobu.');

        if (is_numeric($id_param)) {
            $id = $id_param;
            $kancelar = $this->kancelarManager->get($id);
            $form['name']->setDefaultValue($kancelar->name);
            $form['adresa']->setDefaultValue($kancelar->adresa);
            $form['email']->setDefaultValue($kancelar->email);
            $form['telefon']->setDefaultValue($kancelar->telefon);
            $form['location']->setDefaultValue($kancelar->location);
            $form['locationLink']->setDefaultValue($kancelar->locationLink);
            $form['ico']->setDefaultValue($kancelar->ico);
            $form['bankAccount']->setDefaultValue($kancelar->bankAccount);
            $form['oteviraciDoba']->setDefaultValue($kancelar->oteviraciDoba);

            $form->addHidden('id', $id_param);
        }

        $form->addSubmit('send', 'odeslat');

        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
            try {
                $this->kancelarManager->save($values);
            } catch (DuplicateNameException $e) {
                $form['nazev']->addError('Tento sbor již existuje.');
                return;
            }
            $onSuccess();
        };

        return $form;
    }

}
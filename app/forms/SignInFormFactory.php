<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Security\Passwords;


class SignInFormFactory
{
	use Nette\SmartObject;

	/** @var FormFactory */
	private $factory;

	/** @var User */
	private $user;


	public function __construct(FormFactory $factory, User $user)
	{
		$this->factory = $factory;
		$this->user = $user;
	}


	/**
	 * @return Form
	 */
	public function create(callable $onSuccess)
	{
		$form = $this->factory->create();
		$form->addText('username', 'Login:')
			->setRequired('Prosím vyplńte Login.');

		$form->addPassword('password', 'Heslo:')
			->setRequired('Prosím vyplńte heslo.');

		$form->addCheckbox('remember', 'Pamatuj si mě')
            ->setHtmlAttribute('class', 'loginCheckBox');

		$form->addSubmit('send', 'Přihlásit')
            ->setHtmlAttribute('class', 'loginButton');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			try {
                $values['password'] = Passwords::hash($values->password);
				$this->user->setExpiration($values->remember ? '14 days' : '20 minutes');
				$this->user->login($values->username, $values->password);
			} catch (Nette\Security\AuthenticationException $e) {
				$form->addError('The username or password you entered is incorrect.');
				return;
			}
			$onSuccess();
		};

		return $form;
	}
}

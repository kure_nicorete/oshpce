<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 29.1.18
 * Time: 19:53
 */

namespace App\Forms;

use App\Model\RoleManager;
use App\Model\DuplicateNameException;
use Nette;
use Nette\Application\UI\Form;

class VyborRolesFormFactory {

    use Nette\SmartObject;

    /** @var FormFactory */
    private $factory;

    /**
     * @var RoleManager
     */
    private $roleManager;

    /**
     * VyborRolesFormFactory constructor.
     * @param FormFactory $factory
     * @param RoleManager $roleManager
     */

    public function __construct(FormFactory $factory, RoleManager $roleManager) {
        $this->factory = $factory;
        $this->roleManager = $roleManager;
    }


    public function create($id_param, callable $onSuccess) {
        $form = $this->factory->create();

        $form->addText('nazev', 'Název ')
            ->setRequired('Prosím vyplńte název.');


        if (is_numeric($id_param)) {
            $id = $id_param;
            $role = $this->roleManager->get($id);
            $form['nazev']->setDefaultValue($role->nazev);
            $form->addHidden('id', $id_param);
        }


        $form->addSubmit('send', 'vytvorit');


        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
            try {
                $this->roleManager->save($values);
            } catch (DuplicateNameException $e) {
                $form['nazev']->addError('Tato role již existuje.');
                return;
            }
            $onSuccess();
        };


        return $form;
    }
}
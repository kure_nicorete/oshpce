<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 23:15
 */

namespace App\Model;


use Nette\Database\Context;

class RoleManager extends BaseEntityManager {

    /**
     * RoleManager constructor.
     * @param Context $db
     */
    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName = "role";
    }

    protected function load($row) {
        $ret = new Role();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->nazev = $row['nazev'];

        return $ret;
    }
}
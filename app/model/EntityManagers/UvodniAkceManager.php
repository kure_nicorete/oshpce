<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 28.12.17
 * Time: 19:57
 */

namespace App\Model;


use Nette\Database\Context;

class UvodniAkceManager extends BaseEntityManager {

    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName = 'uvodniAkce';
    }


    protected function load($row) {
        $ret = new UvodniAkce();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->title = $row['title'];
        $ret->path = $row['path'];
        $ret->pathSmall = $row['pathSmall'];
        $ret->zobraz = $row['zobraz'];

        return $ret;
    }
}
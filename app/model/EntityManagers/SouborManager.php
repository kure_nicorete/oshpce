<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 27.12.17
 * Time: 16:17
 */

namespace App\Model;

use Nette\Database\Context;

class SouborManager extends BaseEntityManager {

    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName = 'soubory';
    }


    protected function load($row) {
        $ret = new Soubor();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->nazev = $row['nazev'];
        $ret->fileLocal = $row['fileLocal'];
        $ret->url = $row['url'];
        $ret->path = $row['path'];

        return $ret;
    }
}
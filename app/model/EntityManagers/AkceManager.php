<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 22:32
 */

namespace App\Model;


use Nette\Database\Context;

class AkceManager extends BaseEntityManager {

    /**
     * AkceManager constructor.
     * @param Context $db
     */
    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName = "akce";
    }

    protected function load($row) {
        $ret = new Akce();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->akce = $row['akce'];
        return $ret;
    }
}
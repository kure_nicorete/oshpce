<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 23:19
 */

namespace App\Model;


use Nette\Database\Context;

class HodnostariManager extends BaseEntityManager {

    /**
     * @param RoleManager
     */
    private $roleManager;

    /**
     * HodnostariManager constructor.
     * @param Context $db
     * @param RoleManager $roleManager
     */
    public function __construct(Context $db, RoleManager $roleManager) {
        parent::__construct($db);
        $this->roleManager = $roleManager;
        $this->tableName = 'hodnostari';
    }

    protected function load($row) {
        $ret = new Hodnostari();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->jmeno = $row['jmeno'];
        $ret->prijmeni = $row['prijmeni'];
        $ret->foto = $row['foto'];
        $ret->zobraz = $row['zobraz'];
        $ret->email = $row['email'];
        $ret->telefon = $row['telefon'];
        $ret->sdh = $row['sdh'];
        $ret->role = $this->roleManager->get($row['role']);

        return $ret;
    }
}
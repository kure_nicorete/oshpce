<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 5.1.18
 * Time: 22:36
 */

namespace App\Model;


use Nette\Database\Context;

class SboryManager extends BaseEntityManager {


    /**
     * SboryManager constructor.
     * @param Context $db
     */
    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName='sbory';
    }

    protected function load($row) {
        $ret = new Sbory();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->name = $row['name'];
        $ret->mail = $row['mail'];
        $ret->web = $row['web'];
        $ret->byteArray = $row['bytearray'];

        return $ret;
    }
}
<?php

namespace App\Model;

use Nette\Database\Context;

/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 27.12.17
 * Time: 16:55
 */

class KategorieInfoManager extends BaseEntityManager {

    /**
     * @param KategorieManager
     */
    private $kategorieManager;

    /**
     * @param SouborManager
     */
    private $souboryManager;

    public function __construct(Context $db, KategorieManager $kategorieManager, SouborManager $souboryManager) {
        parent::__construct($db);
        $this->kategorieManager = $kategorieManager;
        $this->souboryManager = $souboryManager;
        $this->tableName = 'kategorieInfo';
    }


    protected function load($row) {
        $ret = new KategorieInfo();
        $ret->soubor = $this->souboryManager->get($row['soubory_id']);
        $ret->kategorie = $this->kategorieManager->get($row['kategorie_id']);
        $ret->subKategorie = $this->kategorieManager->get($row['subkategorie_id']);
        $ret->sekce = $row['sekce'];
        $this->setIdentity($ret, $row['id']);

        return $ret;
    }
}
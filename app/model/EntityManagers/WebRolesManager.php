<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 22:38
 */

namespace App\Model;


use Nette\Database\Context;

class WebRolesManager extends BaseEntityManager {

    /**
     * WebRolesManager constructor.
     * @param Context $db
     */
    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName = "webRoles";
    }

    protected function load($row) {
        $ret = new WebRoles();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->nazev = $row['nazev'];

        return $ret;
    }
}
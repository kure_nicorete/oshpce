<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 23:23
 */

namespace App\Model;


use Nette\Database\Context;

class AuditLogManager extends BaseEntityManager {

    /**
     * @param AkceManager
     */
    private $akceManager;
    /**
     * @param UserManager
     */
    private $userManager;

    /**
     * AuditLogManager constructor.
     * @param Context $db
     * @param AkceManager $akceManager
     * @param UserManager $userManager
     */
    public function __construct(Context $db, AkceManager $akceManager, UserManager $userManager) {
        parent::__construct($db);
        $this->akceManager = $akceManager;
        $this->userManager = $userManager;
        $this->tableName = 'auditLog';
    }


    protected function load($row) {
        $ret = new AuditLog();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->akce = $this->akceManager->get($row['akce_id']);
        $ret->user = $this->userManager->get($row['users_id']);
        $ret->timestamp = $row['timestamp'];

        return $ret;
    }
}
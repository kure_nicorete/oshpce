<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 2.1.18
 * Time: 22:40
 */

namespace App\Model;


use Nette\Database\Context;

class RadaAkceManager extends BaseEntityManager {

    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName = 'radaAkce';
    }

    protected function load($row) {
        $ret = new RadaAkce();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->mistoKonani = $row['mistoKonani'];
        $ret->casKonani = $row['casKonani'];
        $ret->poznamka = $row['poznamka'];

        return $ret;
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 17.12.17
 * Time: 21:44
 */

namespace App\Model;


use Nette\Database\Context;

class LinkToManager extends BaseEntityManager {

    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName = 'linkTo';
    }

    protected function load($row) {
        $ret = new LinkTo();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->title = $row['title'];
        $ret->text = $row['text'];
        $ret->url = $row['url'];
        $ret->special = $row['special'];
        $ret->image = $row['image'];
        $ret->pathActive = $row['pathActive'];
        $ret->pathShadow = $row['pathShadow'];

        return $ret;
    }
}

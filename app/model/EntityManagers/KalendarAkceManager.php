<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 29.12.17
 * Time: 10:50
 */

namespace App\Model;


use Nette\Database\Context;

class KalendarAkceManager extends BaseEntityManager {


    /**
     * KalendarAkceManager constructor.
     * @param Context $db
     */
    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName = 'kalendarAkci';
    }

    protected function load($row) {
        $ret = new KalendarAkce();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->typ = $row['typ'];
        $ret->datumKonani = $row['datumKonani'];
        $ret->okres = $row['okres'];
        $ret->nazev = $row['nazev'];
        $ret->mistoKonani = $row['mistoKonani'];
        $ret->kategorie = $row['kategorie'];
        $ret->discipliny = $row['discipliny'];
        $ret->pravidla = $row['pravidla'];
        $ret->kontakt = $row['kontakt'];
        $ret->poznamka = $row['poznamka'];
        $ret->edited = $row['edited'];

        return $ret;
    }

}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 17.12.17
 * Time: 21:16
 */

namespace App\Model;


use Nette\Database\Context;
use Nette\Object;

/**
 * @property \Nette\Database\Context db
 */
/**
 * @property string tableName
 */

abstract class BaseEntityManager extends Object {

    /** @var \Nette\Database\Context */
    protected $db;

    /** @var string */
    protected $tableName;

    public function __construct(Context $db) {
        $this->db = $db;
    }

    public function get($key) {
        $row = $this->db->table($this->tableName)
            ->where('id', $key)
            ->fetch();
        $ret = $this->load($row);
        return $ret;
    }

    public function save($a) {
        $entity = $this->load($a);
        bdump($entity);
        bdump($a);
        if ($entity->getId() === NULL) {
            $id = $this->db
                ->table($this->tableName)
                ->insert($entity->toArray());
            $this->setIdentity($entity, $id);
        } else {
            $data = $entity->toArray();
            unset($data['id']);
            $this->db->table($this->tableName)
                ->where('id', $entity->getId())
                ->update($entity->toArray());
        }
    }

    public function count(array $cond) {
        return $this->db->table($this->tableName)
            ->where($cond)
            ->count('*');
    }

    public function countBy($cond, $parameters = array()) {
        return $this->db->table($this->tableName)
            ->where($cond, $parameters)
            ->count('*');
    }

    public function max($column){
        return $this->db->table($this->tableName)
            ->max($column);
    }

    public function findBy(array $where, $order = '') {
        $rows = $this->db->table($this->tableName)
            ->where($where)
            ->order($order)
            ->fetchAll();

        $output_array = array();
        foreach ($rows as $row) {
            $output_array[] = $this->load($row);
        }
        return $output_array;
    }

    public function findWithLimitBy($where, $parameters = array(), $order = '', $limit, $offset) {
        $rows = $this->db->table($this->tableName)
            ->where($where, $parameters)
            ->order($order)
            ->limit($limit,$offset)
            ->fetchAll();

        $output_array = array();
        foreach ($rows as $row) {
            $output_array[] = $this->load($row);
        }
        return $output_array;
    }

    public function findLastTwentyBy($where, $parameters = array(), $order = '') {
        $rows = $this->db->table($this->tableName)
            ->where($where, $parameters)
            ->order($order)
            ->limit(20,0)
            ->fetchAll();

        $output_array = array();
        foreach ($rows as $row) {
            $output_array[] = $this->load($row);
        }
        return $output_array;
    }


    public function findAll(){
        $rows = $this->db->table($this->tableName)
            ->fetchAll();

        $output_array = array();
        foreach ($rows as $row) {
            $output_array[] = $this->load($row);
        }
        return $output_array;
   }

    public function find($where, $parameters = array()) {
        $rows = $this->db->table($this->tableName)
            ->where($where, $parameters)
            ->fetchAll();

        $output_array = array();
        foreach ($rows as $row) {
            $output_array[] = $this->load($row);
        }
        return $output_array;
    }

    public function findSpecific($where, $order = ' ') {
        $rows = $this->db->table($this->tableName)
            ->where($where)
            ->order($order)
            ->fetchAll();

        $output_array = array();
        foreach ($rows as $row) {
            $output_array[] = $this->load($row);
        }
        return $output_array;
    }

    public function findOne($where, $parameters = array()) {
        $row = $this->db->table($this->tableName)
            ->where($where, $parameters)
            ->fetch();
        return $this->load($row);
    }

    public function remove($key) {
        return $this->db->table($this->tableName)
            ->where('id', $key)
            ->delete();
    }

    public function removeByColumn($key, $column) {
        return $this->db->table($this->tableName)
            ->where($column, $key)
            ->delete();
    }

    abstract protected function load($row);

    /**
     * TOTAL HACK, pro zmenu ID pri vytvareni noveho radku tak,
     * aby jsem vsude jinde mohl mit atribut 'id' privatni.
     * @param \App\Model\IEntity $item
     * @param int $id
     * @return \App\Model\IEntity
     */
    protected function setIdentity(IEntity $item, $id) {
        $item->id = $id;
        return $item;
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 27.12.17
 * Time: 16:50
 */

namespace App\Model;


use Nette\Database\Context;

class KategorieManager extends BaseEntityManager {

    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName = 'kategorie';
    }


    protected function load($row) {
        $ret = new Kategorie();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->nazev = $row['nazev'];

        return $ret;
    }
    
}
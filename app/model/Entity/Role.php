<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 23:14
 */

namespace App\Model;


class Role extends IEntity {

    /** @var int id  */
    public $id;

    /** @var string */
    public $nazev;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'nazev' => $this->nazev
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
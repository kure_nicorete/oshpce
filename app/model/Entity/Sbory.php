<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 5.1.18
 * Time: 22:36
 */

namespace App\Model;


class Sbory extends IEntity {

    /** @var int id  */
    public $id;
    /** @var string name of the sbor  */
    public $name;
    /** @var string mail  */
    public $mail;
    /** @var string mail  */
    public $web;
    /** @var string info about sbor  */
    public $byteArray;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'name' => $this->name,
            'mail' => $this->mail,
            'web' => $this->web,
            'bytearray' => $this->byteArray
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
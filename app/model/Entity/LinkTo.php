<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 17.12.17
 * Time: 21:34
 */

namespace App\Model;

class LinkTo extends IEntity {
    /** @var int id  */
    public $id;
    /** @var string HTML attribute Title  */
    public $title;
    /** @var string Zobrazovany Text  */
    public $text;
    /** @var string smerovane url  */
    public $url;
    /** @var string ma link obrazek  */
    public $special;
    /** @var string css class  */
    public $image;
    /** @var string path to shadowed image  */
    public $pathShadow;
    /** @var string path to active image  */
    public $pathActive;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'title' => $this->title,
            'text' => $this->text,
            'url' => $this->url,
            'special' => $this->special,
            'image' => $this->image,
            'pathShadow' => $this->pathShadow,
            'pathActive' => $this->pathActive
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}

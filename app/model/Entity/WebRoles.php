<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 22:37
 */

namespace App\Model;


class WebRoles extends IEntity {

    /** @var int id  */
    public $id;
    /** @var string nazev role  */
    public $nazev;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'nazev' => $this->nazev
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
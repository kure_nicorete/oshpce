<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 22:31
 */

namespace App\Model;


class Akce extends IEntity {

    /** @var int id  */
    public $id;
    /** @var string nazev kategorie  */
    public $akce;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'akce' => $this->akce
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
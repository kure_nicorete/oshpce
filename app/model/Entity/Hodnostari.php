<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 23:16
 */

namespace App\Model;


class Hodnostari extends IEntity {

    /** @var int id  */
    public $id;
    /** @var string nazev kategorie */
    public $jmeno;
    /** @var string nazev kategorie */
    public $prijmeni;
    /** @var string nazev kategorie */
    public $foto;
    /** @var string nazev kategorie */
    public $zobraz;
    /** @var string nazev kategorie */
    public $email;
    /** @var string nazev kategorie */
    public $telefon;
    /** @var string nazev kategorie */
    public $sdh;
    /** @var \App\Model\Role */
    public $role;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'jmeno' => $this->jmeno,
            'prijmeni' => $this->prijmeni,
            'foto' => $this->foto,
            'zobraz' => $this->zobraz,
            'email' => $this->email,
            'telefon' => $this->telefon,
            'sdh' => $this->sdh,
            'role_id' => $this->role->getId(),

        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
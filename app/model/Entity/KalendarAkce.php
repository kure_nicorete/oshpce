<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 29.12.17
 * Time: 10:36
 */

namespace App\Model;



class KalendarAkce extends IEntity {

    public $id;

    /** @var string */
    public $typ;

    /** @var timestamp */
    public $datumKonani;

    /** @var string */
    public $okres;

    /** @var string */
    public $nazev;

    /** @var string */
    public $mistoKonani;

    /** @var string */
    public $kategorie;

    /** @var string */
    public $discipliny;

    /** @var string */
    public $pravidla;

    /** @var string */
    public $kontakt;

    /** @var string */
    public $poznamka;

    /** @var string */
    public $edited;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'typ' => $this->typ,
            'datumKonani' => $this->datumKonani,
            'okres' => $this->okres,
            'nazev' => $this->nazev,
            'mistoKonani' => $this->mistoKonani,
            'kategorie' => $this->kategorie,
            'discipliny' => $this->discipliny,
            'pravidla' => $this->pravidla,
            'kontakt' => $this->kontakt,
            'poznamka' => $this->poznamka,
            'edited' => $this->edited,
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
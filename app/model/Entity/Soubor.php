<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 27.12.17
 * Time: 16:18
 */

namespace App\Model;


class Soubor extends IEntity {

    /** @var int id  */
    public $id;
    /** @var string nazev   */
    public $nazev;
    /** @var boolean url vs local image  */
    public $fileLocal;
    /** @var string path pro soubor */
    public $path;
    /** @var string smerovane url  */
    public $url;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'nazev' => $this->nazev,
            'localImage' => $this->fileLocal,
            'path' => $this->path,
            'url' => $this->url
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
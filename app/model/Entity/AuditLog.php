<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 22:40
 */

namespace App\Model;


class AuditLog extends IEntity {

    /** @var int id  */
    public $id;

    /** @var \App\Model\Akce */
    public $akce;

    /** @var \App\Model\User */
    public $user;

    /** @var \DateTime timestamp */
    public $timestamp;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'akce_id' => $this->akce->getId(),
            'users_id' => $this->user->getId(),
            'timestamp' => $this->timestamp
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
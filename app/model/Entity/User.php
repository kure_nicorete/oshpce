<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 18.1.18
 * Time: 22:49
 */

namespace App\Model;


class User extends IEntity {

    /** @var int id  */
    public $id;
    /** @var string login  */
    public $login;
    /** @var string nazev kategorie  */
    public $password;
    /** @var string nazev kategorie  */
    public $firstname;
    /** @var string nazev kategorie  */
    public $surname;
    /** @var string nazev kategorie  */
    public $email;
    /** @var \App\Model\WebRolesManager  */
    public $webRole;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'login' => $this->login,
            'password' => $this->password,
            'firstname' => $this->firstname,
            'surname' => $this->surname,
            'email' => $this->email,
            'webRoles_id' => $this->webRole->getId()
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 28.12.17
 * Time: 19:45
 */

namespace App\Model;


class UvodniAkce extends IEntity {

    /** @var int id  */
    public $id;
    /** @var string cesta k obrazku na disku */
    public $path;
    /** @var string cesta k obrazku na disku pro nahled */
    public $pathSmall;
    /** @var string HTML titulek  */
    public $title;
    /** @var boolean zobrazit obrazek */
    public $zobraz;
    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'path' => $this->path,
            'pathSmall' => $this->pathSmall,
            'title' => $this->title,
            'zobraz' => $this->zobraz,
            );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
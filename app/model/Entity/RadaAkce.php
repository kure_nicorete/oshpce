<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 2.1.18
 * Time: 22:40
 */

namespace App\Model;


class RadaAkce extends IEntity {

    public $id;

    /** @var string Misto konani zasedani */
    public $mistoKonani;

    /** @var timestamp cas konani akce */
    public $casKonani;

    /** @var string kratky popis akce */
    public $poznamka;


    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'mistoKonani' => $this->mistoKonani,
            'casKonani' => $this->casKonani,
            'poznamka' => $this->poznamka
            );
        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
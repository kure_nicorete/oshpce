<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 27.12.17
 * Time: 16:48
 */

namespace App\Model;


class Kategorie extends IEntity {

    /** @var int id  */
    public $id;
    /** @var string nazev kategorie  */
    public $nazev;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'nazev' => $this->nazev,
            );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 17.12.17
 * Time: 21:29
 */

namespace App\Model;


use Nette\Object;

/**
 * @property int id
 */
abstract class IEntity extends Object {

    /**
     * prevede entitu na asociativni pole
     */
    abstract public function toArray();

    /**
     * ziska primarni klic
     */
    abstract public function getId();
}
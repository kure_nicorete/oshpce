<?php

namespace App\Model;

/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 27.12.17
 * Time: 17:34
 */

class KategorieInfo extends IEntity {

    /** @var int id  */
    public $id;

    /** @var \App\Model\Kategorie */
    public $kategorie;

    /** @var \App\Model\Kategorie */
    public $subKategorie;

    /** @var \App\Model\Soubor */
    public $soubor;

    /** @var string */
    public $sekce;



    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'soubory_id' => $this->soubor->getId(),
            'kategorie_id' => $this->kategorie->getId(),
            'subkategorie_id' => $this->subKategorie->getId(),
            'sekce' => $this->sekce
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }
}
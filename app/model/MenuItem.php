<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 7.12.17
 * Time: 21:55
 */

namespace App\Model;


use Nette\Object;
use Nette\Utils\Strings;

class MenuItem extends Object {

    /** @var string */
    private $link;
    /** @var string */
    private $title;
    /** @var string */
    private $ifCurrent;


    /**
     * MenuItem constructor.
     * @param string $link - router link to presenter:action
     * @param string $title - attribute to show more description
     * @param string $ifCurrent - attribute for ifCurrent on presenter
     */
    public function __construct($link = '', $title = '', $ifCurrent = '') {
        $this->link = $link;
        $this->title = $title;
        $this->ifCurrent = $ifCurrent;
    }

    public function getLink()
    {
     return $this->link;
    }
    public function getTitle()
    {
        return $this->title;
    }
    public function getIfCurrent()
    {
        return $this->ifCurrent;
    }
}
<?php

namespace App\Model;

use Nette;
use Nette\Database\Context;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class UserManager extends BaseEntityManager implements Nette\Security\IAuthenticator
{
	use Nette\SmartObject;

    private $webRoleManager;

	public function __construct(Context $db, WebRolesManager $webRolesManager) {
        parent::__construct($db);
        $this->tableName = 'users';
        $this->webRoleManager = $webRolesManager;
	}


	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;

		//$row = $this->database->table($this->tableName)->where('login', $username)->fetch();
        $row = $this->findOne('login', array($username));

		if (!$row || $password == $row->password) {
            throw new Nette\Security\AuthenticationException('Nepodařilo se ověřit zadané údaje.', self::IDENTITY_NOT_FOUND);
        }

		$arr = $row->toArray();
		unset($arr['password']);
		return new Nette\Security\Identity($row->id, $row->webRole->nazev, $arr);
	}


	/**
	 * Adds new user.
	 * @param  string
	 * @param  string
	 * @param  string
	 * @return void
	 * @throws DuplicateNameException
	 */
	public function add($username, $email, $password)
	{
		try {
			$this->database->table(self::TABLE_NAME)->insert([
				self::COLUMN_NAME => $username,
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
				self::COLUMN_EMAIL => $email,
			]);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
	}

    protected function load($row) {
        $ret = new User();
        $ret->login = $row['login'];
        $ret->password = $row['password'];
        $ret->firstname = $row['firstname'];
        $ret->surname = $row['surname'];
        $ret->email = $row['email'];
        $ret->webRole = $this->webRoleManager->get($row['webRoles_id']);
        $this->setIdentity($ret, $row['id']);

        return $ret;
    }
}



class DuplicateNameException extends \Exception
{
}

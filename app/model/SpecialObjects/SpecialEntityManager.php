<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 1.1.18
 * Time: 17:20
 */

namespace App\Model;

/**
 * @property string entityName
 */
use Nette\Database\Context;

abstract class SpecialEntityManager extends BaseEntityManager {

    /**
     * SpecialEntityManager constructor.
     * @param Context $db
     */
    public function __construct(Context $db) {
        parent::__construct($db);
        $this->tableName = 'specialObjects';
    }
}
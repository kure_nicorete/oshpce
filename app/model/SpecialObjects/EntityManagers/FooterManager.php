<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 2.1.18
 * Time: 19:22
 */

namespace App\Model;


use Nette\Database\Context;

class FooterManager extends SpecialEntityManager {

    /**
     * SpecialEntityManager constructor.
     * @param Context $db
     */
    public function __construct(Context $db) {
        parent::__construct($db);
    }

    protected function load($row) {
        $ret = new Footer();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->name = $row['name'];
        $ret->serialized = $row['serialized'];
        $deserialized = $ret->unserialize($row['serialized']);
        $ret->mobil = $deserialized['mobil'];
        $ret->email = $deserialized['email'];
        $ret->pracovniDny = $deserialized['pracovniDny'];

        return $ret;
    }
}
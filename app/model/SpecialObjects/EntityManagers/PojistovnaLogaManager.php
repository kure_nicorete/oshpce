<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 5.1.18
 * Time: 22:04
 */

namespace App\Model;


use Nette\Database\Context;

class PojistovnaLogaManager extends SpecialEntityManager {


    /**
     * PojistovnaLogaManager constructor.
     */
    public function __construct(Context $db) {
        parent::__construct($db);
    }

    protected function load($row) {
        $ret = new PojistovnaLoga();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->name = $row['name'];
        $ret->serialized = $row['serialized'];
        $deserialized = $ret->unserialize($row['serialized']);
        $ret->imagePath = $deserialized['imagePath'];
        $ret->imagePath2 = $deserialized['imagePath2'];

        return $ret;
    }
}
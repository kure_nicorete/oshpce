<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 2.1.18
 * Time: 23:18
 */

namespace App\Model;


use Nette\Database\Context;

class KancelarManager extends SpecialEntityManager {

    /**
     * SpecialEntityManager constructor.
     * @param Context $db
     */
    public function __construct(Context $db) {
        parent::__construct($db);
    }

    /**
     * @param $row
     * @param Kancelar $ret
     * @return void
     */
    public function fillObject($row, Kancelar $ret)
    {
        $ret->adresa = $row['adresa'];
        $ret->email = $row['email'];
        $ret->telefon = $row['telefon'];
        $ret->location = $row['location'];
        $ret->locationLink = $row['locationLink'];
        $ret->ico = $row['ico'];
        $ret->bankAccount = $row['bankAccount'];
        $ret->oteviraciDoba = $row['oteviraciDoba'];
    }

    protected function load($row)
    {
        $ret = new Kancelar();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
            $ret->name = $row['name'];
            if (isset($row['serialized'])) {
                $ret->serialized = $row['serialized'];
                $deserialized = $ret->unserialize($row['serialized']);
                $this->fillObject($deserialized, $ret);
            } else {
                $this->fillObject($row, $ret);
            }
        } else {
            $ret->name = $row['name'];
            $this->fillObject($row, $ret);
        }
        return $ret;
    }
}
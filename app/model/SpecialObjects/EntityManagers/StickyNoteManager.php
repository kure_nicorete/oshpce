<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 1.1.18
 * Time: 17:13
 */

namespace App\Model;


use Nette\Database\Context;

class StickyNoteManager extends SpecialEntityManager {


    /**
     * SpecialEntityManager constructor.
     * @param Context $db
     */
    public function __construct(Context $db) {
        parent::__construct($db);
    }

    protected function load($row) {
        $ret = new StickyNote();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->name = $row['name'];
        $ret->serialized = $row['serialized'];
        $deserialized = $ret->unserialize($row['serialized']);
        $ret->text = $deserialized['text'];

        return $ret;
    }


}
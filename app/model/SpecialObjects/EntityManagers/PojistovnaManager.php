<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 5.1.18
 * Time: 21:49
 */

namespace App\Model;


use Nette\Database\Context;

class PojistovnaManager extends SpecialEntityManager {


    /**
     * PojistovnaManager constructor.
     */
    public function __construct(Context $db) {
        parent::__construct($db);
    }

    protected function load($row) {
        $ret = new Pojistovna();
        if (isset($row['id'])) {
            $ret->id = $row['id'];
        }
        $ret->name = $row['name'];
        $ret->serialized = $row['serialized'];
        $deserialized = $ret->unserialize($row['serialized']);
        $ret->telefon = $deserialized['telefon'];
        $ret->fax = $deserialized['fax'];
        $ret->jmeno = $deserialized['jmeno'];
        $ret->adresa = $deserialized['adresa'];

        return $ret;
    }
}
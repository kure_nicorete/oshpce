<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 30.12.17
 * Time: 21:36
 */

namespace App\Model;


use Serializable;

abstract class ISpecialEntity extends IEntity implements Serializable {

    /** @var int id  */
    public $id;
    /** @var string name of specialized object  */
    public $name;
    /** @var string serializedBlob */
    public $serialized;

    /**
     * prevede entitu na asociativni pole
     */
    public function toArray() {
        $ret = array(
            'name' => $this->name,
            'serialized' => $this->serialize()
        );

        if (isset($this->id)){
            $ret['id']= $this->id;
        }

        return $ret;
    }

    /**
     * ziska primarni klic
     */
    public function getId() {
        return isset($this->id) ? $this->id : NULL;
    }


    abstract function serialize();


    function unserialize($serialized) {
        return unserialize($serialized);
    }

}
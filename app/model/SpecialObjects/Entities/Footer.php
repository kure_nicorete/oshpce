<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 2.1.18
 * Time: 18:26
 */

namespace App\Model;


class Footer extends ISpecialEntity {

    /** @var string email */
    public $email;
    /** @var string mobil */
    public $mobil;
    /** @var string pracovni dny kdy se pracuje oddělené čárkou */
    public $pracovniDny;


    function serialize() {
        $ret = array(
            'email' => $this->email,
            'mobil' => $this->mobil,
            'pracovniDny' => $this->pracovniDny
        );
        return serialize($ret);
    }

}
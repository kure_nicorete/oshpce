<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 5.1.18
 * Time: 22:04
 */

namespace App\Model;


class PojistovnaLoga extends ISpecialEntity {
    /** @var string obrazek1 */
    public $imagePath;
    /** @var string obrazek2 */
    public $imagePath2;

    function serialize() {
        $ret = array(
            'imagePath' => $this->imagePath,
            'imagePath2' => $this->imagePath2
        );
        return serialize($ret);
    }
}
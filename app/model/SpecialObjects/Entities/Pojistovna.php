<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 5.1.18
 * Time: 21:48
 */

namespace App\Model;


class Pojistovna extends ISpecialEntity {
    /** @var string adresa */
    public $adresa;
    /** @var string telefon */
    public $telefon;
    /** @var string fax */
    public $fax;
    /** @var string jmenoPobocky */
    public $jmeno;

    function serialize() {
        $ret = array(
            'adresa' => $this->adresa,
            'telefon' => $this->telefon,
            'fax' => $this->fax,
            'jmeno' => $this->jmeno
        );
        return serialize($ret);
    }
}
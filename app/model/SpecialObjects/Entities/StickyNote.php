<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 30.12.17
 * Time: 21:24
 */

namespace App\Model;

class StickyNote extends ISpecialEntity {

    /** @var string unserialized text */
    public $text;


    function serialize() {
        $ret = array(
            'text' => $this->text
            );
        return serialize($ret);
    }
}
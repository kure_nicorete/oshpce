<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 2.1.18
 * Time: 23:09
 */

namespace App\Model;


class Kancelar extends ISpecialEntity {

    /** @var string adresa */
    public $adresa;
    /** @var string email */
    public $email;
    /** @var string telefon */
    public $telefon;
    /** @var string gps */
    public $location;
    /** @var string link na mapu */
    public $locationLink;
    /** @var string ico */
    public $ico;
    /** @var string ucet */
    public $bankAccount;
    /** @var string ucet */
    public $oteviraciDoba;



    function serialize() {
        $ret = array(
            'adresa' => $this->adresa,
            'email' => $this->email,
            'telefon' => $this->telefon,
            'location' => $this->location,
            'locationLink' => $this->locationLink,
            'ico' => $this->ico,
            'bankAccount' => $this->bankAccount,
            'oteviraciDoba' => $this->oteviraciDoba
        );
        return serialize($ret);
    }
}
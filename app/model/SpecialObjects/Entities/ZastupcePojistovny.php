<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 5.1.18
 * Time: 21:37
 */

namespace App\Model;


class ZastupcePojistovny extends ISpecialEntity {
    /** @var string adresa */
    public $adresa;
    /** @var string email */
    public $email;
    /** @var string telefon */
    public $telefon;
    /** @var string jmeno */
    public $jmeno;

    function serialize() {
        $ret = array(
            'adresa' => $this->adresa,
            'email' => $this->email,
            'telefon' => $this->telefon,
            'jmeno' => $this->jmeno
        );
        return serialize($ret);
    }
}
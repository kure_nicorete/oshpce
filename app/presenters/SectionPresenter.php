<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 28.12.17
 * Time: 10:17
 */

namespace App\Presenters;

use App\Model\Footer;
use App\Model\MenuItem;

abstract class SectionPresenter extends BasePresenter {


    /**
     * @var string enums
     */
    protected $ORDER_BY_SOUBORY_ID_DESC = 'soubory_id DESC';
    protected $FOOTER = 'Footer';

    /**
     * @var string subkategory
     */
    protected $subKategorie;

    /**
     * @var string kategory
     */
    protected $kategorieName;

    /**
     * @var \App\Model\Kategorie kategory
     */
    protected $kategorie;

    /**
     * @inject
     * @var \App\Model\FooterManager footerManager
     */
    public $footerManager;

    abstract function getKategory();


    protected function beforeRender() {
        parent::beforeRender();

        $this->template->footer = $this->footerManager->findOne('name = ?', $this->FOOTER);
        $this->template->menu = array(
            'OSH Pardubice' => new MenuItem('Oshpce:aktualne',"",'Oshpce:*'),
            'ORHS' => new MenuItem('Orhs:aktualne','Odborná rada hasičských soutěží okresu Pardubice','Orhs:*'),
            'ORR' => new MenuItem('Orr:aktualne','Odborná rada represe okresu Pardubice','Orr:*'),
            'ORP' => new MenuItem('Orp:aktualne','Odborná rada prevence okresu Pardubice','Orp:*'),
            'OROO' => new MenuItem('Oroo:aktualne','Odborná rada ochrany obyvatelstva okresu Pardubice','Oroo:*'),
            'ORM' => new MenuItem('Orm:aktualne','Odborná rada mládeže okresu Pardubice', 'Orm:*'),
            'ORH' => new MenuItem('Orh:aktualne','Odborná rada historie okresu Pardubice','Orh:*'),
            'AZH' => new MenuItem('Azh:aktualne','Aktiv zasloužilých hasičů okresu Pardubice','Azh:*'),
            'Archiv' => new MenuItem('Archiv:oshpce','Všechny publikované dokumenty dle kategorií','Archiv:*')
        );

    }
}
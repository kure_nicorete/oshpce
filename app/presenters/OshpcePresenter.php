<?php

namespace App\Presenters;


use App\Forms\KategorieInfoManagerFormFactory;
use App\Model\KalendarAkceManager;
use App\Model\KancelarManager;
use App\Model\KategorieInfoManager;
use App\Model\KategorieManager;
use App\Model\LinkToManager;
use App\Model\MenuItem;
use App\Model\PojistovnaLogaManager;
use App\Model\PojistovnaManager;
use App\Model\RadaAkceManager;
use App\Model\SboryManager;
use App\Model\SouborManager;
use App\Model\StickyNoteManager;
use App\Model\User;
use App\Model\UserManager;
use App\Model\UvodniAkceManager;
use App\Model\ZastupcePojistovnyManager;
use Nette\Utils\FileSystem;

class OshpcePresenter extends SectionPresenter {

    /**
     * @var string enums
     */
    private $ORDER_BY_ID_ASC = 'id ASC';
    private $ORDER_BY_ID_DESC = 'id DESC';
    private $ORDER_BY_NAME_ASC = 'name ASC';
    private $STICKY_NOTE = 'StickyNote';
    private $KANCELAR = 'KancelarOSH';
    private $POJISTOVAK = 'ZastupcePojistovny';
    private $POJISTOVNA = 'Pojistovna';
    private $POJISTOVNA_LOGA = 'PojistovnaLoga';
    private $AKTUALNE = "Aktuálně";
    private $HROMADA = "Volební výroční valné hromady SDH a okrsků";
    private $VYSLEDKY = "Výsledky soutěží";
    private $ZAKONIK = "Novela Občanského Zákoníku";
    private $ZASEDANI = "Zasedání představitelů SDH v Černé za Bory";
    private $OSH = "OSH PCE";

    private $ADD_CATEGORY;
    private $ADD_SECTION;

    /**
     * @inject
     * @var LinkToManager
     */
    public $linkToManager;

    /**
     * @inject
     * @var UvodniAkceManager
     */
    public $uvodniAkceManager;

    /**
     * @inject
     * @var KategorieManager
     */
    public $kategorieManager;

    /**
     * @inject
     * @var KategorieInfoManager
     */
    public $kategorieInfoManager;

    /**
     * @inject
     * @var KalendarAkceManager
     */
    public $kalendarAkciManager;

    /**
     * @inject
     * @var RadaAkceManager
     */
    public $radaAkceManager;

    /**
     * @inject
     * @var KancelarManager
     */
    public $kancelarManager;

    /**
     * @inject
     * @var ZastupcePojistovnyManager
     */
    public $zastupcePojistovnyManager;

    /**
     * @inject
     * @var PojistovnaManager
     */
    public $pojistovnaManager;

    /**
     * @inject
     * @var PojistovnaLogaManager
     */
    public $pojistovnaLogaManager;

    /**
     * @inject
     * @var StickyNoteManager
     */
    public $stickyNoteManager;

    /**
     * @inject
     * @var SboryManager
     */
    public $sboryManager;

    /**
     * @inject
     * @var UserManager
     */
    public $userManager;

    /**
     * @inject
     * @var SouborManager
     */
    public $souboryManager;

    /** @var KategorieInfoManagerFormFactory */
    private $katgorieFormFactory;

    public function __construct(KategorieInfoManagerFormFactory $infoManagerFormFactory) {
        $this->katgorieFormFactory = $infoManagerFormFactory;
    }

    protected function beforeRender() {
        parent::beforeRender();
        $this->kategorieName = $this->getKategory();
        $this->kategorie = $this->kategorieManager->findOne('nazev=?', $this->kategorieName);

        $this->template->subMenu = array(
            'Aktuálně' => new MenuItem('Oshpce:aktualne', '', 'Oshpce:aktualne'),
            'Tiskopisy' => new MenuItem('Oshpce:tiskopisy', '', 'Oshpce:tiskopisy'),
            'VV OSH' => new MenuItem('Oshpce:vvosh', 'Výkonný výbor OSH', 'Oshpce:vvosh'),
            'Zpravodaj' => new MenuItem('Oshpce:zpravodaj', '', 'Oshpce:zpravodaj'),
            'HVP' => new MenuItem('Oshpce:hvp', 'Pojištění', 'Oshpce:hvp'),
            'Kontakty' => new MenuItem('Oshpce:kontakty', '', 'Oshpce:kontakty'),
            'Administrace' => new MenuItem('Admin:login', 'Přihlášení do administrace', 'Admin:login'),
        );
    }

    public function renderAktualne(){
        $this->subKategorie = 'Aktuálně';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->page = $this->subKategorie;
        $this->template->stickyNote = $this->stickyNoteManager->findOne('name = ?', $this->STICKY_NOTE);

        $this->template->updateDate = $this->kalendarAkciManager->max('edited');

        $this->template->kalendarAkci = $this->kalendarAkciManager->findSpecific(
            'datumKonani BETWEEN current_date() AND DATE_ADD(current_date(), INTERVAL 21 DAY)',
            $this->ORDER_BY_ID_DESC);

        $this->template->zasedaniRady = $this->radaAkceManager->findSpecific(
            'casKonani BETWEEN current_date() AND DATE_ADD(current_date(), INTERVAL 21 DAY)',
            $this->ORDER_BY_ID_DESC);

        $this->template->aktualne = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ? AND sekce=?',
            array($this->kategorie->id, $subKategorie->id, $this->AKTUALNE), $this->ORDER_BY_SOUBORY_ID_DESC);
        $this->template->hromada = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ? AND sekce=?',
            array($this->kategorie->id, $subKategorie->id, $this->HROMADA), $this->ORDER_BY_SOUBORY_ID_DESC);
        $this->template->vysledky = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ? AND sekce=?',
            array($this->kategorie->id, $subKategorie->id, $this->VYSLEDKY), $this->ORDER_BY_SOUBORY_ID_DESC);
        $this->template->zakonik = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ? AND sekce=?',
            array($this->kategorie->id, $subKategorie->id, $this->ZAKONIK), $this->ORDER_BY_SOUBORY_ID_DESC);
        $this->template->zasedani = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ? AND sekce=?',
            array($this->kategorie->id, $subKategorie->id, $this->ZASEDANI), $this->ORDER_BY_SOUBORY_ID_DESC);
        $this->template->links = $this->linkToManager->findBy(array(), $this->ORDER_BY_ID_ASC);
        $this->template->imageLinks = $this->linkToManager->findBy(array('special = 1 '), $this->ORDER_BY_ID_ASC);
        $this->template->uvodniAkce = $this->uvodniAkceManager->findBy(array('zobraz = 1 '), $this->ORDER_BY_ID_ASC);
    }

    public function renderAddDocument($subCategory, $sekce = null) {
        $this->ADD_CATEGORY = $subCategory;
        $this->ADD_SECTION = $sekce;
    }

    public function actionEditDocument($id){

    }

    public function handleDeleteDocument($id){
        $soubor = $this->souboryManager->get($id);
        if($soubor->fileLocal == 1){
            $fileToDelete = WWW_DIR .'/'. $soubor->path;
            FileSystem::delete($fileToDelete);
        }
        $this->kategorieInfoManager->removeByColumn($id, "soubory_id");
        $this->souboryManager->remove($id);
        $this->flashMessage('Item byl úspěšně odstraněn.');
    }

    protected function createComponentKategorieForm() {
        return $this->katgorieFormFactory->create($this->OSH, $this->ADD_CATEGORY, $this->ADD_SECTION, function ($subCategory) {
            $this->redirect('Oshpce:'.strval($subCategory));
        });
    }

    public function renderTiskopisy(){
        $this->subKategorie = 'Tiskopisy';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->tiskopisy = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderVvosh(){
        $this->subKategorie = 'VV OSH';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->vvosh = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderKontakty(){
        $this->template->sbory = $this->sboryManager->findBy(array(), $this->ORDER_BY_NAME_ASC);

//        $ret = new User();
//        $ret->login = "kure";
//        $ret->password = sha1("lamenicoj1088");
//        $ret->firstname = "Kure";
//        $ret->surname = "Cerman";
//        $ret->email = "kure@kure.cz";
//        $ret->webRole = 1;
//
//        $this->userManager->save($ret);

    }

    public function renderHvp(){
        $this->subKategorie = 'HVP';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->kancelar = $this->kancelarManager->findOne('name = ?', $this->KANCELAR);
        $this->template->zastupcePojistovny = $this->zastupcePojistovnyManager->findOne('name = ?', $this->POJISTOVAK);
        $this->template->pojistovna = $this->pojistovnaManager->findOne('name = ?', $this->POJISTOVNA);
        $this->template->pojistovnaLoga = $this->pojistovnaLogaManager->findOne('name = ?', $this->POJISTOVNA_LOGA);

        $this->template->hvp = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderZpravodaj(){
        $this->subKategorie = 'Zpravodaj';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->zpravodaj = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    function getKategory() {
        return "OSH PCE";
    }

}

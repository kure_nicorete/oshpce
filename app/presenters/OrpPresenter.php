<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 1.1.18
 * Time: 20:29
 */

namespace App\Presenters;


use App\Model\MenuItem;

class OrpPresenter extends SectionPresenter {

    /**
     * @inject
     * @var \App\Model\KategorieManager
     */
    public $kategorieManager;

    /**
     * @inject
     * @var \App\Model\KategorieInfoManager
     */
    public $kategorieInfoManager;

    /**
     * @inject
     * @var \App\Model\KalendarAkceManager
     */
    public $kalendarAkciManager;


    protected function beforeRender() {
        parent::beforeRender();
        $this->kategorieName = $this->getKategory();
        $this->kategorie = $this->kategorieManager->findOne('nazev=?', $this->kategorieName);

        $this->template->subMenu = array(
            'Aktuálně' => new MenuItem('Orp:aktualne', '', 'Orp:aktualne'),
            'Tiskopisy' => new MenuItem('Orp:tiskopisy', '', 'Orp:tiskopisy'),
            'Odbornosti' => new MenuItem('Orp:odbornosti', 'Vše k odbornosti Preventista', 'Orp:odbornosti'),
            'PVČ' => new MenuItem('Orp:pvc', 'Preventivně výchovná činnost', 'Orp:pvc'),
            'PO očima dětí' => new MenuItem('Orp:ocima', '', 'Orp:ocima'));
    }

    public function renderAktualne() {
        $this->subKategorie = 'Aktuálně';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->aktualne = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderTiskopisy() {
        $this->subKategorie = 'Tiskopisy';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->tiskopisy = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderOdbornosti() {
        $this->subKategorie = 'Odbornosti';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->odbornosti = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderPvc() {
        $this->subKategorie = 'PVČ';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->pvc = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderOcima() {
        $this->subKategorie = 'PO očima dětí';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->ocima = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    function getKategory() {
        return 'ORP';
    }
}
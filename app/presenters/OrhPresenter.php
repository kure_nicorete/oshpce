<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 1.1.18
 * Time: 20:59
 */

namespace App\Presenters;


use App\Model\MenuItem;

class OrhPresenter extends SectionPresenter {

    /**
     * @inject
     * @var \App\Model\KategorieManager
     */
    public $kategorieManager;

    /**
     * @inject
     * @var \App\Model\KategorieInfoManager
     */
    public $kategorieInfoManager;

    /**
     * @inject
     * @var \App\Model\KalendarAkceManager
     */
    public $kalendarAkciManager;

    protected function beforeRender() {
        parent::beforeRender();
        $this->kategorieName = $this->getKategory();
        $this->kategorie = $this->kategorieManager->findOne('nazev=?', $this->kategorieName);

        $this->template->subMenu = array(
            'Aktuálně' => new MenuItem('Orh:aktualne', '', 'Orh:aktualne'),
            'Tiskopisy' => new MenuItem('Orh:tiskopisy', '', 'Orh:tiskopisy'));
    }

    public function renderAktualne() {
        $this->subKategorie = 'Aktuálně';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->aktualne = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderTiskopisy() {
        $this->subKategorie = 'Tiskopisy';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->tiskopisy = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    function getKategory() {
        return 'ORH';
    }
}
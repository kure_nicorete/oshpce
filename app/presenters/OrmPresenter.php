<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 1.1.18
 * Time: 20:47
 */

namespace App\Presenters;


use App\Model\MenuItem;

class OrmPresenter extends SectionPresenter {

    /**
     * @inject
     * @var \App\Model\KategorieManager
     */
    public $kategorieManager;

    /**
     * @inject
     * @var \App\Model\KategorieInfoManager
     */
    public $kategorieInfoManager;

    /**
     * @inject
     * @var \App\Model\KalendarAkceManager
     */
    public $kalendarAkciManager;

    protected function beforeRender() {
        parent::beforeRender();
        $this->kategorieName = $this->getKategory();
        $this->kategorie = $this->kategorieManager->findOne('nazev=?', $this->kategorieName);

        $this->template->subMenu = array(
            'Aktuálně' => new MenuItem('Orm:aktualne', '', 'Orm:aktualne'),
            'Tiskopisy' => new MenuItem('Orm:tiskopisy', '', 'Orm:tiskopisy'),
            'Přípravky' => new MenuItem('Orm:pripravky', '', 'Orm:pripravky'),
            'Plamen' => new MenuItem('Orm:plamen', '', 'Orm:plamen'),
            'Dorost' => new MenuItem('Orm:dorost', '', 'Orm:dorost'),
            'LMH' => new MenuItem('Orm:lmh', 'Liga Mladých Hasičů okresu Pardubice', 'Orm:lmh'),
            'Učíme se' => new MenuItem('Orm:ucimese', 'Podklady ke vzdělávání', 'Orm:ucimese'));
    }

    public function renderAktualne() {
        $this->subKategorie = 'Aktuálně';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->aktualne = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderTiskopisy() {
        $this->subKategorie = 'Tiskopisy';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->tiskopisy = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderPripravky() {
        $this->subKategorie = 'Přípravky';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->pripravky = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderPlamen() {
        $this->subKategorie = 'Plamen';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->plamen = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderDorost() {
        $this->subKategorie = 'Dorost';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->dorost = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }
    public function renderLmh() {
        $this->subKategorie = 'LMH';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->lmh = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderUcimese() {
        $this->subKategorie = 'Učíme se';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->ucimese = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    function getKategory() {
        return 'ORM';
    }
}
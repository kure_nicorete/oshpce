<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 1.1.18
 * Time: 19:28
 */

namespace App\Presenters;


use App\Model\MenuItem;

class OrhsPresenter extends  SectionPresenter {

    /**
     * @inject
     * @var \App\Model\KategorieManager
     */
    public $kategorieManager;

    /**
     * @inject
     * @var \App\Model\KategorieInfoManager
     */
    public $kategorieInfoManager;

    /**
     * @inject
     * @var \App\Model\KalendarAkceManager
     */
    public $kalendarAkciManager;


    protected function beforeRender() {
        parent::beforeRender();
        $this->kategorieName = $this->getKategory();
        $this->kategorie = $this->kategorieManager->findOne('nazev=?', $this->kategorieName);

        $this->template->subMenu = array(
            'Aktuálně' => new MenuItem('Orhs:aktualne', '', 'Orhs:aktualne'),
            'Tiskopisy' => new MenuItem('Orhs:tiskopisy', '', 'Orhs:tiskopisy'),
            'Plamen' => new MenuItem('Orhs:plamen', '', 'Orhs:plamen'),
            'Dorost' => new MenuItem('Orhs:dorost', '', 'Orhs:dorost'),
            'Požární Sport' => new MenuItem('Orhs:sport', '', 'Orhs:sport'));
    }

    public function renderAktualne() {
        $this->subKategorie = 'Aktuálně';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->aktualne = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderTiskopisy() {
        $this->subKategorie = 'Tiskopisy';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->tiskopisy = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderPlamen() {
        $this->subKategorie = 'Plamen';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->plamen = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderDorost() {
        $this->subKategorie = 'Dorost';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->dorost = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderSport() {
        $this->subKategorie = 'Požární sport';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->sport = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    function getKategory() {
        return 'ORHS';
    }
}
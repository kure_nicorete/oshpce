<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 1.1.18
 * Time: 20:13
 */

namespace App\Presenters;


use App\Model\MenuItem;

class OrrPresenter extends SectionPresenter {

    /**
     * @inject
     * @var \App\Model\KategorieManager
     */
    public $kategorieManager;

    /**
     * @inject
     * @var \App\Model\KategorieInfoManager
     */
    public $kategorieInfoManager;

    /**
     * @inject
     * @var \App\Model\KalendarAkceManager
     */
    public $kalendarAkciManager;

    protected function beforeRender() {
        parent::beforeRender();
        $this->kategorieName = $this->getKategory();
        $this->kategorie = $this->kategorieManager->findOne('nazev=?', $this->kategorieName);

        $this->template->subMenu = array(
            'Aktuálně' => new MenuItem('Orr:aktualne', '', 'Orr:aktualne'),
            'Tiskopisy' => new MenuItem('Orr:tiskopisy', '', 'Orr:tiskopisy'),
            'Odbornosti' => new MenuItem('Orr:odbornosti', 'Vše k odbornosti Hasič', 'Orr:odbornosti'),
            'Legislativa' => new MenuItem('Orr:legislativa', '', 'Orr:legislativa'),
            'JSDHO' => new MenuItem('Orr:jsdho', 'Jednotky sborů dobrovolných hasičů', 'Orr:jsdho'));
    }

    public function renderAktualne() {
        $this->subKategorie = 'Aktuálně';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->aktualne = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderTiskopisy() {
        $this->subKategorie = 'Tiskopisy';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->tiskopisy = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderOdbornosti() {
        $this->subKategorie = 'Odbornosti';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->odbornosti = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderLegislativa() {
        $this->subKategorie = 'Legislativa';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->legislativa = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    public function renderJsdho() {
        $this->subKategorie = 'JSDHO';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $this->template->jsdho = $this->kategorieInfoManager->findLastTwentyBy(
            'kategorie_id = ? AND subkategorie_id = ?',
            array($this->kategorie->id, $subKategorie->id), $this->ORDER_BY_SOUBORY_ID_DESC);
    }

    function getKategory() {
        return 'ORR';
    }
}
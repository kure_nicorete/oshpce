<?php

namespace App\Presenters;

use App\Forms;
use App\Model\HodnostariManager;
use App\Model\KancelarManager;
use App\Model\MenuItem;
use App\Model\PojistovnaManager;
use App\Model\RoleManager;
use App\Model\SboryManager;
use Nette\Application\UI\Form;


class AdminPresenter extends SectionPresenter
{
	/** @var Forms\SignInFormFactory */
	private $signInFactory;

	/** @var Forms\SignUpFormFactory */
	private $signUpFactory;
	/** @var Forms\KategorieInfoManagerFormFactory */
	private $katgorieFormFactory;

    /** @var Forms\VyborRolesFormFactory */
    private $vyborRolesFormFactory;

    /** @var RoleManager */

    private $roleManager;
    /**
     * @var HodnostariManager
     */
    private $hodnostarManager;
    /**
     * @var Forms\HodnostariFormFactory
     */
    private $hodnostariFormFactory;

    /**
     * @var SboryManager
     */
    private $sboryManager;
    /**
     * @var Forms\KancelarFormFactory
     */
    private $sboryFormFactory;
    /**
     * @var KancelarManager
     */
    private $kancelarManager;
    /**
     * @var PojistovnaManager
     */
    private $pojistovnaManager;
    /**
     * @var Forms\KancelarFormFactory
     */
    private $kancelarFormFactory;


    public function __construct(Forms\KategorieInfoManagerFormFactory $infoManagerFormFactory,
                                Forms\SignInFormFactory               $signInFactory,
                                Forms\SignUpFormFactory               $signUpFactory,
                                Forms\VyborRolesFormFactory           $vyborRoleFactory,
                                Forms\HodnostariFormFactory           $hodnostariFormFactory,
                                Forms\KancelarFormFactory             $kancelarFormFactory,
                                RoleManager                           $roleManager,
                                HodnostariManager                     $hodnostariManager,
                                SboryManager                          $sboryManager,
                                PojistovnaManager                     $pojistovnaManager,
                                KancelarManager                       $kancelarManager

    ) {
	    $this->katgorieFormFactory = $infoManagerFormFactory;
		$this->signInFactory = $signInFactory;
		$this->signUpFactory = $signUpFactory;
        $this->vyborRolesFormFactory = $vyborRoleFactory;
        $this->hodnostariFormFactory = $hodnostariFormFactory;
        $this->roleManager = $roleManager;
        $this->hodnostarManager = $hodnostariManager;
        $this->sboryManager = $sboryManager;
        $this->kancelarManager = $kancelarManager;
        $this->pojistovnaManager = $pojistovnaManager;
        $this->kancelarFormFactory = $kancelarFormFactory;
    }


	/**
	 * Sign-in form factory.
	 * @return Form
	 */
	protected function createComponentSignInForm()
	{
		return $this->signInFactory->create(function (){
			$this->redirect('Oshpce:aktualne');
		});
	}

    public function actionNewRole($id = null) {}

    public function actionNewHodnostar($id = null) {}

    public function actionNewSbor($id = null) {}

    public function actionNewHvp($id = null) {}

    public function actionNewKancelar($id = null) {}

    public function handleDeleteItem($id) {
        if ($this->roleManager->remove($id)) {
            $this->flashMessage('Role byla odstraněna.');
        } else {
            $this->flashMessage('Role neexistuje.', 'warning');
        }
        $this->redirect('this');
    }

    public function handleDeleteHodnostar($id) {
        if ($this->hodnostarManager->remove($id)) {
            $this->flashMessage('Role byla odstraněna.');
        } else {
            $this->flashMessage('Role neexistuje.', 'warning');
        }
        $this->redirect('this');
    }

    public function handleDeleteSbor($id) {
        if ($this->sboryManager->remove($id)) {
            $this->flashMessage('Sbor byl odstraněna.');
        } else {
            $this->flashMessage('Sbor neexistuje.', 'warning');
        }
        $this->redirect('this');
    }

    public function handleDeleteKancelar($id) {
        if ($this->kancelarManager->remove($id)) {
            $this->flashMessage('Kancelář byla odstraněna.');
        } else {
            $this->flashMessage('Kancelář neexistuje.', 'warning');
        }
        $this->redirect('this');
    }

    public function handleDeleteHvp($id) {
        if ($this->pojistovnaManager->remove($id)) {
            $this->flashMessage('Pojištovna byla odstraněna.');
        } else {
            $this->flashMessage('Pojištovna neexistuje.', 'warning');
        }
        $this->redirect('this');
    }

    protected function createComponentAddNewRole(){
        return $this->vyborRolesFormFactory->create($this->getParameter("id"), function (){
            $this->flashMessage("Ulozeno");
            $this->redirect('Admin:vybor');
        });
    }

    protected function createComponentAddNewHodnostar(){
        return $this->hodnostariFormFactory->create($this->getParameter("id"), function (){
            $this->flashMessage("Ulozeno");
            $this->redirect('Admin:vybor');
        });
    }

    protected function createComponentAddNewSbor(){
        return $this->sboryFormFactory->create($this->getParameter("id"), function (){
            $this->flashMessage("Ulozeno");
            $this->redirect('Admin:vybor');
        });
    }

    protected function createComponentKategorieForm(){
        return $this->katgorieFormFactory->create($this->kategory, $this->subKategory, $this->sekce,function () {
            $this->redirect('Oshpce:aktualne');
        });
    }

    protected function createComponentAddNewKancelar(){
        return $this->kancelarFormFactory->create($this->getParameter("id"), function () {
            $this->redirect('Oshpce:aktualne');
        });
    }


	/**
	 * Sign-up form factory.
	 * @return Form
	 */
	protected function createComponentSignUpForm()
	{
		return $this->signUpFactory->create(function () {
			$this->redirect('Homepage:');
		});
	}


	public function actionLogin(){
	}

	public function actionLogout()
	{
		$this->getUser()->logout();
	}


    protected function beforeRender() {
        parent::beforeRender();

        $login = $this->getUser()->isLoggedIn() ? array('Odhlasit' => new MenuItem('Admin:logout', '', 'Admin:logout')) : array('Přihlásit' => new MenuItem('Admin:login', '', 'Admin:login'));
        $this->template->subMenu = array(
            'Listeček' => new MenuItem('Orm:aktualne', '', 'Orm:aktualne'),
            'Kancelář' => new MenuItem('Admin:kancelar', '', 'Admin:kancelar'),
            'Výbor' => new MenuItem('Admin:vybor', '', 'Admin:vybor'),
            'Sbory' => new MenuItem('Admin:sbory', '', 'Admin:sbory'),

            );
    }

    public function renderVybor(){
        $this->template->roles = $this->roleManager->findAll();
        $this->template->hodnostari = $this->hodnostarManager->findAll();
    }

    public function renderSbory(){
        $this->template->sbory = $this->sboryManager->findAll();
    }

    public function renderHvp(){
        $this->template->pojistovna = $this->pojistovnaManager->findAll();
    }

    public function renderkancelar(){
        $this->template->kancelare = $this->kancelarManager->findAll();
    }

    function getKategory() {
        return "login";
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: kure
 * Date: 1.1.18
 * Time: 21:14
 */

namespace App\Presenters;


use App\Model\MenuItem,
    IPub\VisualPaginator\Components as VisualPaginator;

class ArchivPresenter extends SectionPresenter {

    /**
     * @inject
     * @var \App\Model\KategorieManager
     */
    public $kategorieManager;

    /**
     * @inject
     * @var \App\Model\KategorieInfoManager
     */
    public $kategorieInfoManager;

    /**
     * @inject
     * @var \App\Model\KalendarAkceManager
     */
    public $kalendarAkciManager;

    protected function beforeRender() {
        parent::beforeRender();

        $this->template->subMenu = array(
            'OSH Pardubice' => new MenuItem('Archiv:oshpce',"",'Archiv:oshpce'),
            'ORHS' => new MenuItem('Archiv:orhs','Odborná rada hasičských soutěží okresu Pardubice','Archiv:orhs'),
            'ORR' => new MenuItem('Archiv:orr','Odborná rada represe okresu Pardubice','Archiv:orr'),
            'ORP' => new MenuItem('Archiv:orp','Odborná rada prevence okresu Pardubice','Archiv:orp'),
            'OROO' => new MenuItem('Archiv:oroo','Odborná rada ochrany obyvatelstva okresu Pardubice','Archiv:oroo'),
            'ORM' => new MenuItem('Archiv:orm','Odborná rada mládeže okresu Pardubice', 'Archiv:orm'),
            'ORH' => new MenuItem('Archiv:orh','Odborná rada historie okresu Pardubice','Archiv:orh'),
            'AZH' => new MenuItem('Archiv:azh','Aktiv zasloužilých hasičů okresu Pardubice','Archiv:azh'));
    }

    public function renderOshpce() {
        $this->subKategorie = 'OSH PCE';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $itemsCount = $this->kategorieInfoManager->countBy(
            'kategorie_id = ?',
            $subKategorie->id);

        $paginator = $this->getPaginator($itemsCount);
        $this->template->oshpce = $this->kategorieInfoManager->findWithLimitBy(
            'kategorie_id = ?',
            $subKategorie->id, $this->ORDER_BY_SOUBORY_ID_DESC,
            $paginator->itemsPerPage,
            $paginator->offset);
    }
    
    public function renderOrhs() {
        $this->subKategorie = 'ORHS';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $itemsCount = $this->kategorieInfoManager->countBy(
            'kategorie_id = ?',
            $subKategorie->id);

        $paginator = $this->getPaginator($itemsCount);
        $this->template->orhs = $this->kategorieInfoManager->findWithLimitBy(
            'kategorie_id = ?',
            $subKategorie->id, $this->ORDER_BY_SOUBORY_ID_DESC,
            $paginator->itemsPerPage,
            $paginator->offset);

    }
    public function renderOrr() {
        $this->subKategorie = 'ORR';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $itemsCount = $this->kategorieInfoManager->countBy(
            'kategorie_id = ?',
            $subKategorie->id);

        $paginator = $this->getPaginator($itemsCount);
        $this->template->orr = $this->kategorieInfoManager->findWithLimitBy(
            'kategorie_id = ?',
            $subKategorie->id, $this->ORDER_BY_SOUBORY_ID_DESC,
            $paginator->itemsPerPage,
            $paginator->offset);
    }
    public function renderOrp() {
        $this->subKategorie = 'ORP';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $itemsCount = $this->kategorieInfoManager->countBy(
            'kategorie_id = ?',
            $subKategorie->id);

        $paginator = $this->getPaginator($itemsCount);
        $this->template->orp = $this->kategorieInfoManager->findWithLimitBy(
            'kategorie_id = ?',
            $subKategorie->id, $this->ORDER_BY_SOUBORY_ID_DESC,
            $paginator->itemsPerPage,
            $paginator->offset);
    }
    public function renderOroo() {
        $this->subKategorie = 'OROO';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $itemsCount = $this->kategorieInfoManager->countBy(
            'kategorie_id = ?',
            $subKategorie->id);

        $paginator = $this->getPaginator($itemsCount);
        $this->template->oroo = $this->kategorieInfoManager->findWithLimitBy(
            'kategorie_id = ?',
            $subKategorie->id, $this->ORDER_BY_SOUBORY_ID_DESC,
            $paginator->itemsPerPage,
            $paginator->offset);
    }
    public function renderOrm() {
        $this->subKategorie = 'ORM';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $itemsCount = $this->kategorieInfoManager->countBy(
            'kategorie_id = ?',
            $subKategorie->id);

        $paginator = $this->getPaginator($itemsCount);
        $this->template->orm = $this->kategorieInfoManager->findWithLimitBy(
            'kategorie_id = ?',
            $subKategorie->id, $this->ORDER_BY_SOUBORY_ID_DESC,
            $paginator->itemsPerPage,
            $paginator->offset);
    }

    public function renderOrh() {
        $this->subKategorie = 'ORH';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $itemsCount = $this->kategorieInfoManager->countBy(
            'kategorie_id = ?',
            $subKategorie->id);

        $paginator = $this->getPaginator($itemsCount);
        $this->template->orh = $this->kategorieInfoManager->findWithLimitBy(
            'kategorie_id = ?',
            $subKategorie->id, $this->ORDER_BY_SOUBORY_ID_DESC,
            $paginator->itemsPerPage,
            $paginator->offset);
    }

    public function renderAzh() {
        $this->subKategorie = 'AZH';
        $subKategorie = $this->kategorieManager->findOne('nazev=?', $this->subKategorie);

        $itemsCount = $this->kategorieInfoManager->countBy(
            'kategorie_id = ?',
            $subKategorie->id);

        $paginator = $this->getPaginator($itemsCount);
        $this->template->azh = $this->kategorieInfoManager->findWithLimitBy(
            'kategorie_id = ?',
            $subKategorie->id, $this->ORDER_BY_SOUBORY_ID_DESC,
            $paginator->itemsPerPage,
            $paginator->offset);
    }


    private function getPaginator($itemsCount) {
        // Get visual paginator components
        $visualPaginator = $this['visualPaginator'];
        // Get paginator form visual paginator
        $paginator = $visualPaginator->getPaginator();
        // Define items count per one page
        $paginator->itemsPerPage = 30;
        // Define total items in list
        $paginator->itemCount = $itemsCount;

        return $paginator;
    }

    /**
     * Create items paginator
     *
     * @return VisualPaginator\Control
     */
    protected function createComponentVisualPaginator()
    {
        // Init visual paginator
        $control = new VisualPaginator\Control;

        return $control;
    }

    function getKategory() {
        return 'ARCHIV';
    }
}
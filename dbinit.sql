-- MySQL Script generated by MySQL Workbench
-- Sat Apr 30 16:17:39 2022
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema oshpce
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema oshpce
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `oshpce` DEFAULT CHARACTER SET utf8 ;
USE `oshpce` ;

-- -----------------------------------------------------
-- Table `oshpce`.`webRoles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`webRoles` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`webRoles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nazev` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nazev_UNIQUE` (`nazev` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`users` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(255) NOT NULL,
  `password` VARCHAR(75) NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `webRoles_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`login` ASC),
  INDEX `fk_users_webRoles1_idx` (`webRoles_id` ASC),
  CONSTRAINT `fk_users_webRoles1`
    FOREIGN KEY (`webRoles_id`)
    REFERENCES `oshpce`.`webRoles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`role` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nazev` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nazev_UNIQUE` (`nazev` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`hodnostari`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`hodnostari` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`hodnostari` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `jmeno` VARCHAR(100) NOT NULL,
  `prijmeni` VARCHAR(100) NOT NULL,
  `foto` VARCHAR(100) NOT NULL,
  `zobraz` TINYINT(1) NOT NULL DEFAULT 0,
  `email` VARCHAR(100) NULL DEFAULT '',
  `telefon` VARCHAR(45) NULL DEFAULT '',
  `sdh` VARCHAR(100) NULL DEFAULT '',
  `role_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hodnostari_role1_idx` (`role_id` ASC),
  CONSTRAINT `fk_hodnostari_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `oshpce`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`soubory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`soubory` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`soubory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fileLocal` TINYINT(1) NOT NULL,
  `nazev` VARCHAR(255) NOT NULL,
  `path` VARCHAR(255) NULL,
  `url` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`kategorie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`kategorie` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`kategorie` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nazev` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`kalendarAkci`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`kalendarAkci` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`kalendarAkci` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `typ` VARCHAR(100) NOT NULL,
  `datumKonani` TIMESTAMP NOT NULL,
  `okres` VARCHAR(100) NOT NULL,
  `nazev` VARCHAR(100) NOT NULL,
  `mistoKonani` VARCHAR(100) NOT NULL,
  `kategorie` VARCHAR(100) NULL,
  `discipliny` VARCHAR(100) NULL,
  `pravidla` VARCHAR(100) NULL,
  `kontakt` VARCHAR(100) NULL,
  `poznamka` VARCHAR(100) NULL,
  `edited` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`akce`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`akce` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`akce` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `akce` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `akce_UNIQUE` (`akce` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`auditLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`auditLog` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`auditLog` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `akce_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  `timestamp` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_auditLog_akce1_idx` (`akce_id` ASC),
  INDEX `fk_auditLog_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_auditLog_akce1`
    FOREIGN KEY (`akce_id`)
    REFERENCES `oshpce`.`akce` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_auditLog_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `oshpce`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`specialObjects`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`specialObjects` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`specialObjects` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `serialized` BLOB NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`kategorieInfo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`kategorieInfo` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`kategorieInfo` (
  `soubory_id` INT NOT NULL,
  `kategorie_id` INT NOT NULL,
  `subkategorie_id` INT NOT NULL,
  `sekce` VARCHAR(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`soubory_id`, `kategorie_id`, `subkategorie_id`),
  INDEX `fk_soubory_has_kategorie_kategorie1_idx` (`kategorie_id` ASC),
  INDEX `fk_soubory_has_kategorie_soubory1_idx` (`soubory_id` ASC),
  INDEX `fk_kategorie_kategorie1_idx` (`subkategorie_id` ASC),
  CONSTRAINT `fk_soubory_has_kategorie_soubory1`
    FOREIGN KEY (`soubory_id`)
    REFERENCES `oshpce`.`soubory` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_soubory_has_kategorie_kategorie1`
    FOREIGN KEY (`kategorie_id`)
    REFERENCES `oshpce`.`kategorie` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_kategorie_kategorie1`
    FOREIGN KEY (`subkategorie_id`)
    REFERENCES `oshpce`.`kategorie` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`radaAkce`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`radaAkce` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`radaAkce` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mistoKonani` VARCHAR(100) NOT NULL,
  `casKonani` TIMESTAMP NOT NULL,
  `poznamka` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`sbory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`sbory` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`sbory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `mail` VARCHAR(100) NOT NULL DEFAULT '',
  `web` VARCHAR(100) NOT NULL DEFAULT '',
  `bytearray` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`linkTo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`linkTo` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`linkTo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `text` VARCHAR(100) NOT NULL,
  `url` VARCHAR(100) NOT NULL,
  `image` VARCHAR(100) NOT NULL,
  `special` TINYINT(1) NOT NULL DEFAULT 0,
  `pathShadow` VARCHAR(100) NULL DEFAULT '',
  `pathActive` VARCHAR(100) NULL DEFAULT '',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `oshpce`.`uvodniAkce`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oshpce`.`uvodniAkce` ;

CREATE TABLE IF NOT EXISTS `oshpce`.`uvodniAkce` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(100) NOT NULL,
  `pathSmall` VARCHAR(100) NOT NULL,
  `title` VARCHAR(100) NOT NULL,
  `zobraz` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

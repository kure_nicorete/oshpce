
INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'OSH Pardubice na sociální síti Facebook',
'Facebook',
'http://www.facebook.com/profile.php?id=100002911228884&ref=tn_tnmn',
'icofb',
1,
'/images/icons/fb-shadow.gif',
'/images/icons/fb-color.gif'
);
INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Sdružení hasičů Čech, Moravy a Slezska',
'SH ČMS',
'http://dh.cz/',
'icoshcms',
1,
'/images/icons/shcms-shadow.gif',
'/images/icons/shcms-color.gif'
);

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Fotky pořízené na akcích OSH Pardubice',
'Rajce.net',
'http://orhoshpce.rajce.idnes.cz/',
'icorajce',
1,
'/images/icons/rajce-shadow.gif',
'/images/icons/rajce-color.gif'
);



INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Požární bezpečnost s.r.o - náš partner',
'Požární bezpečnost s.r.o',
'http://www.po-bp.cz/',
'icopobp',
1,
'/images/icons/pobp-shadow.gif',
'/images/icons/pobp-color.gif'
);

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Česká rada dětí a mládeže',
'Česká rada dětí a mládeže',
'http://www.crdm.cz/',
'icocrdm',
1,
'/images/icons/crdm-shadow.gif',
'/images/icons/crdm-color.gif'
);

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Hasičská vzájemná pojišťovna a.s. - náš partner',
'Hasičská vzájemná pojišťovna a.s.',
'http://www.hvp.cz/',
'icohvp',
1,
'/images/icons/hvp-shadow.gif',
'/images/icons/hvp-color.gif'
);

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Ohnisko žhavých zpráv',
'POŽÁRY.cz',
'http://www.pozary.cz/',
'icopozary',
1,
'/images/icons/pozary-shadow.gif',
'/images/icons/pozary-color.gif'
);

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Výroba vlajek, praporů, znaků, vlaječek, odznaků a symbolů',
'ALERION s.r.o.',
'http://www.alerion.cz/',
'icoalerion',
1,
'/images/icons/alerion-shadow.gif',
'/images/icons/alerion-color.gif'
);

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Oficiální informační server pardubického krajského sdružení hasičů Čech, Moravy a Slezka',
'KSH ČMS Pardubického kraje',
'http://www.kshpak.cz/',
'',
0,
'',
''
);

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Hasičský záchranný sbor Pardubického kraje',
'HZS Pardubického kraje',
'http://www.hzscr.cz/hzs-pardubickeho-kraje.aspx',
'',0,'','');

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Požární sport',
'Firesport',
'http://www.firesport.eu/',
'',0,'','');

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Výroba požární techniky',
'Pavliš a Hartmann, spol. s.r.o.',
'http://www.phhp.cz/',
'',0,'','');

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Liga okresu Pardubice v požárním útoku družstev',
'LPO',
'http://lpo.estranky.cz/',
'',0,'','');

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Vše o požárních hlásičích',
'Požární hlásiče',
'http://www.hlasic-pozaru.cz/',
'',0,'','');

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Česká hasičská jednota',
'ČHJ',
'http://www.hasicskajednota.cz/',
'',0,'','');

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Moravská hasičská jednota, o.s.',
'MHJ',
'http://www.mhj.cz/',
'',0,'','');

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Ústřední hasičská škola v Bílých Poličanech',
'UHS Bílé Poličany',
'http://www.uhsbp.cz/',
'',0,'','');

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Hasičský týdeník',
'Hasičský týdeník',
'http://hasicskytydenik.cz/hasicsky-tydenik/archiv',
'',0,'','');

INSERT INTO oshpce.linkTo 
(`title`,`text`,`url`,`image`,`special`,`pathShadow`,`pathActive`) VALUES
(
'Česká asociace hasičských důstojníků',
'ČAHD',
'"http://www.cahd.cz/',
'',0,'','');


INSERT INTO `oshpce`.`soubory`
(`fileLocal`,`nazev`,`path`,`url`)
VALUES
(0,
'Tiskopis přehledu o majetku a závazcích nebo jednoduché účetní závěrky',
'',
'https://www.dh.cz/images/Dokumenty/ekonomika/ucetnictvi/mp-4-2016/Prilohy-pro-jednoduch-etnictv.xls'
);

INSERT INTO `oshpce`.`soubory`
(`fileLocal`,`nazev`,`path`,`url`)
VALUES
(1,
'Tiskopis přehledu o majetku a závazcích nebo jednoduché účetní závěrky',
'archiv/osh/tiskopisy/2017_zadost_vstup_evidence_sdh.xlsx',
''
);

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('OSH PCE');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('ORHS');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('ORR');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('ORP');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('OROO');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('ORM');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('ORH');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('AZH');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('ARCHIV');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Aktuálně');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Tiskopisy');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('VV OSH');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Rezervace');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Zpravodaj');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('HVP');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Kontakty');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Plamen');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Dorost');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Požární sport');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Odbornosti');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Legislativa');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('JSDHO');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('PVČ');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('PO očima dětí');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('OSH PCE');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Přípravky');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('LMH');

INSERT INTO `oshpce`.`kategorie`
(`nazev`)
VALUES
('Učíme se');


INSERT INTO `oshpce`.`kategorieInfo`
(`soubory_id`,
`kategorie_id`,
`subkategorie_id`,
`sekce`)
VALUES
(1,
1,
10,
'Aktuálně');

INSERT INTO `oshpce`.`kategorieInfo`
(`soubory_id`,
`kategorie_id`,
`subkategorie_id`,
`sekce`)
VALUES
(2,
1,
10,
'Aktuálně');

INSERT INTO `oshpce`.`uvodniAkce`
(`path`,
`pathSmall`,
`title`,
`zobraz`)
VALUES
("images/uvodniAkce/Klub_slapacich_moskvicu",
"images/uvodniAkce/Klub_slapacich_moskvicu_small",
"Klub šlapacich moskvičů",
1);


INSERT INTO `oshpce`.`kalendarAkci`
(`typ`,`datumKonani`,`okres`,`nazev`,`mistoKonani`,`kategorie`,`discipliny`,`pravidla`,`kontakt`,`poznamka`,`edited`)
VALUES
('soutez',
CURRENT_TIME(),
'HK',
'Slavnosti snezenek',
'Vsesportovni stadion',
'dy, di, ž, m',
'ZPV',
'SHS',
'havran.jj@seznam.cz;;Jiří Janda',
'bambucha',
CURRENT_TIME()
);

INSERT INTO `oshpce`.`kalendarAkci`
(`typ`,`datumKonani`,`okres`,`nazev`,`mistoKonani`,`kategorie`,`discipliny`,`pravidla`,`kontakt`,`poznamka`,`edited`)
VALUES
('soutez',
CURRENT_TIME(),
'HK',
'Slavnosti snezenek',
'Vsesportovni stadion',
'dy, di, ž, m',
'ZPV',
'SHS',
'osmark@seznam.cz;tel.: 724 977 537;Markéta Oprchalská',
'bambucha',
CURRENT_TIME()
);

INSERT INTO `oshpce`.`kalendarAkci`
(`typ`,`datumKonani`,`okres`,`nazev`,`mistoKonani`,`kategorie`,`discipliny`,`pravidla`,`kontakt`,`poznamka`,`edited`)
VALUES
('soutez',
CURRENT_TIME(),
'HK',
'Slavnosti snezenek',
'Vsesportovni stadion',
'',
'',
'',
'',
'',
CURRENT_TIME()
);



INSERT INTO `oshpce`.`radaAkce`
(`casKonani`,`mistoKonani`,`poznamka`)
VALUES
(
CURRENT_TIME(),
'HorniDolni@',
'Zasedani rady bla bla'
);

INSERT INTO `oshpce`.`radaAkce`
(`casKonani`,`mistoKonani`,`poznamka`)
VALUES
(
CURRENT_TIME(),
'HorniDolniasdasd',
'Zasedani rady bla bla'
);

INSERT INTO `oshpce`.`sbory`
(`name`,
`mail`,
`web`,
`bytearray`)
VALUES
('Horní Roveň',
'rasaklibor@seznam.cz',
'',
'01010100');

INSERT INTO `oshpce`.`webRoles`
(`nazev`)
VALUES
("Admin");

INSERT INTO `oshpce`.`webRoles`
(`nazev`)
VALUES
("Asistentka");


INSERT INTO `oshpce`.`users`
(`login`,
`password`,
`firstname`,
`surname`,
`email`,
`webRoles_id`)
VALUES
(
"kure",
"$2y$10$Yy4AJR3i7lpgrozW6rvpNuir8XIYhxU6ZD30kovJYPOszyd2mbnTG",
"Karel",
"Cerman",
"kure@kureci.com",
1);



